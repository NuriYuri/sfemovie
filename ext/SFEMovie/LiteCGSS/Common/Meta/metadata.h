#ifndef L_METADATA_HEADER
#define L_METADATA_HEADER

namespace cgss {
	namespace meta {
		template <class T>
		struct Log {
			static constexpr auto classname = "Object";
		};
	}
}

#endif