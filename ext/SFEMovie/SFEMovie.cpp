#include "rb_common.h"
#include "Texture_Bitmap.h"

VALUE rb_cMovie = Qnil;
VALUE rb_cBitmap = Qnil;
VALUE rb_eRGSSError = Qnil;

void rb_Movie_Free(void* data)
{
  sfe::Movie* pdata = reinterpret_cast<sfe::Movie*>(data);
  if(pdata != nullptr) { delete pdata; }
}

VALUE rb_Movie_Alloc(VALUE klass)
{
  sfe::Movie* data = new sfe::Movie();
  return Data_Wrap_Struct(klass, NULL, rb_Movie_Free, data);
}

VALUE rb_MopenFromFile(VALUE self, VALUE filename) {
  Check_Type(filename, T_STRING);
  RB_GET_DATA(sfe::Movie, Qfalse);
  return data->openFromFile(StringValueCStr(filename)) ? Qtrue : Qfalse;
}

VALUE rb_Mplay(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qnil);

  data->play();

  return Qnil;
}

VALUE rb_Mpause(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qnil);

  data->pause();

  return Qnil;
}

VALUE rb_Mstop(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qnil);

  data->stop();

  return Qnil;
}

VALUE rb_Mplaying(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qfalse);
  return data->getStatus() == sfe::Status::Playing ? Qtrue : Qfalse;
}

VALUE rb_Mpaused(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qfalse);
  return data->getStatus() == sfe::Status::Paused ? Qtrue : Qfalse;
}

VALUE rb_Mstopped(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qfalse);
  return data->getStatus() == sfe::Status::Stopped ? Qtrue : Qfalse;
}


VALUE rb_Mupdate(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qnil);

  data->update();

  return Qnil;
}

VALUE rb_MgetDuration(VALUE self) {
  RB_GET_DATA(sfe::Movie, LONG2NUM(0));

  sf::Time time = data->getDuration();
  
  return DBL2NUM(static_cast<double>(time.asSeconds()));
}

VALUE rb_MsetVolume(VALUE self, VALUE pitch) {
  RB_GET_DATA(sfe::Movie, Qnil);

  data->setVolume(static_cast<float>(NUM2DBL(pitch)));

  return Qnil;
}

VALUE rb_MgetVolume(VALUE self) {
  RB_GET_DATA(sfe::Movie, LONG2FIX(0));

  return DBL2NUM(static_cast<double>(data->getVolume()));
}

VALUE rb_MsetPlayingOffset(VALUE self, VALUE offset) {
  RB_GET_DATA(sfe::Movie, Qnil);
  
  data->setPlayingOffset(sf::seconds(static_cast<float>(NUM2DBL(offset))));

  return Qnil;
}

VALUE rb_MgetPlayingOffset(VALUE self) {
  RB_GET_DATA(sfe::Movie, LONG2NUM(0));

  return DBL2NUM(static_cast<double>(data->getPlayingOffset().asSeconds()));
}

VALUE rb_MgetChannelCount(VALUE self) {
  RB_GET_DATA(sfe::Movie, LONG2NUM(0));
  
  return UINT2NUM(data->getChannelCount());
}

VALUE rb_MgetSampleRate(VALUE self) {
  RB_GET_DATA(sfe::Movie, LONG2NUM(0));
  
  return UINT2NUM(data->getSampleRate());
}

VALUE rb_MgetFrameRate(VALUE self) {
  RB_GET_DATA(sfe::Movie, LONG2NUM(0));
  
  return DBL2NUM(static_cast<double>(data->getFramerate()));
}

VALUE rb_MgetSize(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qnil);

  auto pos = data->getSize();

  VALUE ary = rb_ary_new_capa(2);
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.x)));
  rb_ary_push(ary, DBL2NUM(static_cast<double>(pos.y)));

  return ary;
}

VALUE rb_Mfit(int argc, VALUE* argv, VALUE self) {
  RB_GET_DATA(sfe::Movie, Qnil);
  VALUE x, y, width, height, preserveRatio;
  rb_scan_args(argc, argv, "41", &x, &y, &width, &height, &preserveRatio);

  data->fit(
    static_cast<float>(NUM2DBL(x)),
    static_cast<float>(NUM2DBL(y)),
    static_cast<float>(NUM2DBL(width)),
    static_cast<float>(NUM2DBL(height)),
    RTEST(preserveRatio)
  );

  return Qnil;
}

VALUE rb_MgetCurrentImage(VALUE self) {
  RB_GET_DATA(sfe::Movie, Qnil);

  auto image = data->getCurrentImage();
	VALUE bmp = rb_obj_alloc(rb_cBitmap);
	sf::Texture& bmpTexture = rb::Get<TextureElement>(bmp)->raw();
	bmpTexture = image;
	return bmp;
}

VALUE rb_mUpdateBitmap(VALUE self, VALUE bitmap) {
  RB_GET_DATA(sfe::Movie, Qnil);
  if (rb_obj_is_kind_of(bitmap, rb_cBitmap) != Qtrue) {
    return Qnil;
  }
  auto image = data->getCurrentImage();
	sf::Texture& bmpTexture = rb::Get<TextureElement>(bitmap)->raw();
	bmpTexture = image;
  return Qnil;
}

extern "C" {
  void Init_SFEMovie() {
    auto rb_mLiteRGSS = rb_const_get(rb_cObject, rb_intern("LiteRGSS"));
    rb_cBitmap = rb_const_get(rb_mLiteRGSS, rb_intern("Bitmap"));
	  rb_eRGSSError = rb_const_get(rb_mLiteRGSS, rb_intern("Error"));
    VALUE rb_mSFE = rb_define_module("SFE");
    rb_cMovie = rb_define_class_under(rb_mSFE, "Movie", rb_cObject);
    rb_define_alloc_func(rb_cMovie, rb_Movie_Alloc);

    rb_define_method(rb_cMovie, "open_from_file", _rbf rb_MopenFromFile, 1);
    rb_define_method(rb_cMovie, "play", _rbf rb_Mplay, 0);
    rb_define_method(rb_cMovie, "pause", _rbf rb_Mpause, 0);
    rb_define_method(rb_cMovie, "stop", _rbf rb_Mstop, 0);
    rb_define_method(rb_cMovie, "playing?", _rbf rb_Mplaying, 0);
    rb_define_method(rb_cMovie, "paused?", _rbf rb_Mpaused, 0);
    rb_define_method(rb_cMovie, "stopped?", _rbf rb_Mstopped, 0);
    rb_define_method(rb_cMovie, "update", _rbf rb_Mupdate, 0);
    rb_define_method(rb_cMovie, "get_duration", _rbf rb_MgetDuration, 0);
    rb_define_method(rb_cMovie, "set_volume", _rbf rb_MsetVolume, 1);
    rb_define_method(rb_cMovie, "get_volume", _rbf rb_MgetVolume, 0);
    rb_define_method(rb_cMovie, "set_playing_offset", _rbf rb_MsetPlayingOffset, 1);
    rb_define_method(rb_cMovie, "get_playing_offset", _rbf rb_MgetPlayingOffset, 0);
    rb_define_method(rb_cMovie, "get_channel_count", _rbf rb_MgetChannelCount, 0);
    rb_define_method(rb_cMovie, "get_sample_rate", _rbf rb_MgetSampleRate, 0);
    rb_define_method(rb_cMovie, "get_frame_rate", _rbf rb_MgetFrameRate, 0);
    rb_define_method(rb_cMovie, "get_size", _rbf rb_MgetSize, 0);
    rb_define_method(rb_cMovie, "fit", _rbf rb_Mfit, -1);
    rb_define_method(rb_cMovie, "get_current_texture", _rbf rb_MgetCurrentImage, 0);
    rb_define_method(rb_cMovie, "update_bitmap", _rbf rb_mUpdateBitmap, 1);
  }
}
