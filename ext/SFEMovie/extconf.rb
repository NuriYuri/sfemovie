# frozen_string_literal: true

require 'mkmf'

ext_name = 'SFEMovie'

have_library('sfemovie')
have_library('sfml-system')
have_library('sfml-graphics')
have_library('LiteCGSS_engine')

ASSEMBLE_CXX << ' -std=c++17'
COMPILE_CXX << ' -std=c++17'

create_makefile(ext_name)
