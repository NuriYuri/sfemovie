#ifndef L_UTILS_RUBY_COMMON_HEADER
#define L_UTILS_RUBY_COMMON_HEADER

#include "ruby.h"
#include "sfeMovie/Movie.hpp"
#define _rbf (VALUE (*)(...))

#define RB_PROTECT_SELF(ret) if(RDATA(self)->data == nullptr) \
    return ret; \

#define RB_GET_DATA(type, ret) \
  type* data; \
  RB_PROTECT_SELF(ret) \
  Data_Get_Struct(self, type, data);

extern VALUE rb_cMovie;

#endif
